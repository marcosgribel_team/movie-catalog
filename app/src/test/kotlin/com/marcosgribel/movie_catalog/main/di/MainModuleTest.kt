package com.marcosgribel.movie_catalog.main.di

import com.marcosgribel.movie_catalog.core.di.TestSchedulerProvider
import com.marcosgribel.movie_catalog.core.di.module.DatabaseModuleTest
import com.marcosgribel.movie_catalog.core.di.module.ServiceModuleTest
import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import com.marcosgribel.movie_catalog.genre.model.service.GenreService
import com.marcosgribel.movie_catalog.main.presentation.ui.MainActivity
import com.marcosgribel.movie_catalog.main.presentation.preseneter.MainPresenter
import com.marcosgribel.movie_catalog.main.presentation.preseneter.MainPresenterImpl
import com.marcosgribel.movie_catalog.main.presentation.ui.MainView
import com.marcosgribel.movie_catalog.main.model.usecase.MainUseCase
import com.marcosgribel.movie_catalog.main.model.usecase.MainUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@Module
class MainModuleTest {


    @Provides
    internal fun provideMainView(activity: MainActivity): MainView = activity


    @Provides
    internal fun provideMainUseCase(): MainUseCase {
        return MainUseCaseImpl(ServiceModuleTest().provideGenreService(),
                DatabaseModuleTest().provideGenreDao())

    }

    @Provides
    internal fun provideMainPresenter(view: MainView,
                                      useCase: MainUseCase,
                                      @TestSchedulerProvider scheduler: ScheduleProvider): MainPresenter = MainPresenterImpl(view, useCase, scheduler)

}