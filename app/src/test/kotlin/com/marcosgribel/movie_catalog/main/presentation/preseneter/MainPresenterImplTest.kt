package com.marcosgribel.movie_catalog.main.presentation.preseneter

import com.marcosgribel.movie_catalog.core.TestScheduler
import com.marcosgribel.movie_catalog.core.di.TestSchedulerProvider
import com.marcosgribel.movie_catalog.core.util.AppMock
import com.marcosgribel.movie_catalog.main.di.MainModule
import com.marcosgribel.movie_catalog.main.di.MainModuleTest
import com.marcosgribel.movie_catalog.main.presentation.ui.MainView
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.*

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class MainPresenterImplTest {

    private var mainViewMock = mock(MainView::class.java)
    private var mainPresenterMock = MainModuleTest().provideMainUseCase()


    private lateinit var presenter: MainPresenter


    @Before
    fun setUp(){

        presenter = MainPresenterImpl(mainViewMock, mainPresenterMock, TestScheduler())

    }


    @Test
    fun loadPreConfigurationSuccess(){
        presenter.loadPreConfiguration()
        verify(mainViewMock, times(1)).startMovieActivity()

    }

    @Test
    fun handleErrorSuccess(){
        presenter.handleGlobalError(AppMock.any())
        verify(mainViewMock, times(1)).showGlobalErrorMessage()
    }

}