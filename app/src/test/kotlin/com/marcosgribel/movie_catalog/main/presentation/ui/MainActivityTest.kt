package com.marcosgribel.movie_catalog.main.presentation.ui

import android.content.Intent
import android.widget.ImageView
import android.widget.ProgressBar
import com.marcosgribel.movie_catalog.App
import com.marcosgribel.movie_catalog.AppTest
import com.marcosgribel.movie_catalog.BuildConfig
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivity
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.android.synthetic.main.activity_main.view.*
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import kotlin.text.Typography.times

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = AppTest::class)
class MainActivityTest {


    private lateinit var mainActivity: MainActivity

    @Before
    fun setUp(){
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)
    }


    @Test
    fun assertElementsOnScreen(){
        var imageView = mainActivity.findViewById<ImageView>(R.id.img_logo_main_screen)
        var progressBar = mainActivity.findViewById<ProgressBar>(R.id.progress_main_screen)

        assertNotNull(imageView)
        assertNotNull(progressBar)

    }

    @Test
    fun assertStartMovieActivity(){

        mainActivity.startMovieActivity()
        val expected = Intent(mainActivity, ListMovieActivity::class.java)
        val actual = shadowOf(RuntimeEnvironment.application).nextStartedActivity

        assertEquals(expected.component, actual.component)
    }
}