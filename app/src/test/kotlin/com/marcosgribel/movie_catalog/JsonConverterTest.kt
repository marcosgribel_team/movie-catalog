package com.marcosgribel.movie_catalog

import com.marcosgribel.movie_catalog.core.util.AppMock
import org.junit.Assert
import org.junit.Test

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class JsonConverterTest {


    @Test
    fun test_genre_json_conversion() {

        val fakeGenre = AppMock.fakeGenreResponse()

        Assert.assertNotNull(AppMock.fakeGenreResponse())
        Assert.assertEquals(true, fakeGenre?.genres?.isNotEmpty())
    }


    @Test
    fun test_movie_json_conversion(){

        val fakeMovieResponse = AppMock.fakeMoviePage1Response()

        Assert.assertNotNull(fakeMovieResponse)
        Assert.assertNotNull(fakeMovieResponse?.results)

        var movie = fakeMovieResponse?.results!![0]

        Assert.assertNotNull(movie)
        Assert.assertNotNull(movie.title)
        Assert.assertNotNull(movie.backdropPath)
        Assert.assertNotNull(movie.overview)

    }

}