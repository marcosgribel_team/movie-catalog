package com.marcosgribel.movie_catalog

import com.marcosgribel.movie_catalog.core.di.DaggerAppComponent
import com.marcosgribel.movie_catalog.core.di.DaggerAppComponentTest
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */


class AppTest : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return  return  DaggerAppComponentTest.builder()
                .application(this)
                .build()
    }

}