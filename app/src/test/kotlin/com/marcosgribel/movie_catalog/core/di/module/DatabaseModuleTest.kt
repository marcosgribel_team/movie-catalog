package com.marcosgribel.movie_catalog.core.di.module

import android.app.Application
import com.marcosgribel.movie_catalog.core.model.db.AppDatabase
import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.core.model.db.dao.MovieDao
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import com.marcosgribel.movie_catalog.core.util.AppMock
import dagger.Module
import dagger.Provides
import org.mockito.ArgumentMatcher
import org.mockito.ArgumentMatchers
import org.mockito.Matchers
import org.mockito.Mockito
import org.mockito.Mockito.*
import javax.inject.Singleton


/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@Module
class DatabaseModuleTest {


    @Provides
    @Singleton
    internal fun provideAppDatabase(app: Application): AppDatabase {
        return mock(AppDatabase::class.java)
    }

    @Provides
    @Singleton
    internal fun provideMovieDao(): MovieDao {
        var movieDao = mock(MovieDao::class.java)
        return movieDao
    }


    @Provides
    @Singleton
    internal fun provideGenreDao(): GenreDao {

        var genreDao = mock(GenreDao::class.java)
        doNothing().`when`(genreDao).insert(AppMock.any())
        return genreDao
    }


}