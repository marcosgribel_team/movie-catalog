package com.marcosgribel.movie_catalog.core.di

import com.marcosgribel.movie_catalog.core.TestScheduler
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@Module
class AppModuleTest {


    @Provides
    @Singleton
    @AppLanguage
    fun provideLanguage(): String {
        return "en"
    }

    @Provides
    @Singleton
    @TestSchedulerProvider
    fun provideScheduler() : ScheduleProvider = TestScheduler()



}