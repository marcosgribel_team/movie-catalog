package com.marcosgribel.movie_catalog.core.util

import com.marcosgribel.movie_catalog.JsonConverterTest
import com.marcosgribel.movie_catalog.core.model.entity.GenreResponse
import com.marcosgribel.movie_catalog.core.model.entity.MovieResponse
import com.squareup.moshi.Moshi
import io.reactivex.Maybe
import org.mockito.Mockito

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
object AppMock {

    private var moshi = Moshi.Builder().build()


    internal fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }
    internal fun <T> uninitialized(): T = null as T


    internal fun fakeGenreResponse(): GenreResponse? {

        val inputStream = JsonConverterTest::class.java.classLoader.getResourceAsStream("genres.json")
        val genreJson = FileUtil.readTextStream(inputStream)

        val genreJsonAdapter = moshi.adapter(GenreResponse::class.java)
        val fakeGenreResponse = genreJsonAdapter.fromJson(genreJson)

        return fakeGenreResponse

    }

    internal fun fakeGenreResponseObservable(): Maybe<GenreResponse> {
        return Maybe.create {
            it.onSuccess(fakeGenreResponse()!!)
            it.onComplete()
        }
    }

    internal fun fakeMoviePage1Response(): MovieResponse? {

        val movieInputStream = JsonConverterTest::class.java.classLoader.getResourceAsStream("movies_page_1.json")
        val movieJson = FileUtil.readTextStream(movieInputStream)

        val movieJsonAdapter = moshi.adapter(MovieResponse::class.java)

        return movieJsonAdapter.fromJson(movieJson)
    }

    internal fun fakeMoviePage2Response(): MovieResponse? {

        val movieInputStream = JsonConverterTest::class.java.classLoader.getResourceAsStream("movies_page_2.json")
        val movieJson = FileUtil.readTextStream(movieInputStream)

        val movieJsonAdapter = moshi.adapter(MovieResponse::class.java)

        return movieJsonAdapter.fromJson(movieJson)
    }


    internal fun fakeMoviePage1ResponseObservable(): Maybe<MovieResponse> {
        return Maybe.create {
            it.onSuccess(fakeMoviePage1Response()!!)
            it.onComplete()
        }
    }


    internal fun fakeMoviePage2ResponseObservable(): Maybe<MovieResponse> {
        return Maybe.create {
            it.onSuccess(fakeMoviePage2Response()!!)
            it.onComplete()
        }
    }

}