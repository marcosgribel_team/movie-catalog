package com.marcosgribel.movie_catalog.core.di

import com.marcosgribel.movie_catalog.main.presentation.ui.MainActivity
import com.marcosgribel.movie_catalog.main.di.MainModule
import com.marcosgribel.movie_catalog.main.di.MainModuleTest
import com.marcosgribel.movie_catalog.movie.di.ListMovieModule
import com.marcosgribel.movie_catalog.movie.di.ListMovieModuleTest
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
abstract class ActivityBuilderTest {

    @ContributesAndroidInjector(modules = [
        MainModuleTest::class
    ])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [
        ListMovieModuleTest::class
    ])
    internal abstract fun bindListMovieActivity(): ListMovieActivity

}