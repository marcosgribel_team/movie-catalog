package com.marcosgribel.movie_catalog.core.di.module

import com.marcosgribel.movie_catalog.core.util.AppMock
import com.marcosgribel.movie_catalog.genre.model.service.GenreApi
import com.marcosgribel.movie_catalog.genre.model.service.GenreService
import com.marcosgribel.movie_catalog.genre.model.service.GenreServiceImpl
import com.marcosgribel.movie_catalog.movie.model.service.MovieApi
import com.marcosgribel.movie_catalog.movie.model.service.MovieService
import com.marcosgribel.movie_catalog.movie.model.service.MovieServiceImpl
import dagger.Module
import dagger.Provides
import org.junit.Test

import org.junit.Assert.*
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.*

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@Module
class ServiceModuleTest {

    val apiKey = "xxx"
    val language = "en-US"


    @Provides
    internal fun provideMovieService(): MovieService {
        var movieApi = mock(MovieApi::class.java)

        val fakeListMovie = arrayListOf(AppMock.fakeMoviePage1Response(), AppMock.fakeMoviePage2Response())

        `when`(movieApi.getMovies(apiKey, ArgumentMatchers.anyInt(), language, null))
                .thenReturn(AppMock.fakeMoviePage1ResponseObservable())

        `when`(movieApi.getMovies(apiKey, 2, language, null))
                .thenReturn(AppMock.fakeMoviePage2ResponseObservable())

        `when`(movieApi.searchMovie(apiKey, 2, "query", language))
                .thenReturn(AppMock.fakeMoviePage1ResponseObservable())

        return MovieServiceImpl(movieApi, apiKey, language)
    }

    @Provides
    internal fun provideGenreService(): GenreService {
        var genreApi = mock(GenreApi::class.java)

        `when`(genreApi.getGenres(apiKey, language)).thenReturn(
                AppMock.fakeGenreResponseObservable()
        )

        return GenreServiceImpl(genreApi, apiKey, language)
    }

}