package com.marcosgribel.movie_catalog.core.util

import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

/**
 * Created by marcosgribel on 6/20/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class DateUtilTest {


    @Test
    fun testGetYearFromDateString() {
        val year = DateUtil.getYearFromDateString("2013-10-29", DateUtil.YYYY_MM_DD)
        Assert.assertEquals(2013, year)

    }
}