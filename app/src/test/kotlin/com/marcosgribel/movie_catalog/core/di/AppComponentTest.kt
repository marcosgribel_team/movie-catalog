package com.marcosgribel.movie_catalog.core.di

import android.app.Application
import com.marcosgribel.movie_catalog.AppTest
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModuleTest::class,
    ActivityBuilderTest::class
])
interface AppComponentTest : AndroidInjector<AppTest> {


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponentTest

    }

}