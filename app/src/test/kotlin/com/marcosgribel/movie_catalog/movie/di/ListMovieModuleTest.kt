package com.marcosgribel.movie_catalog.movie.di

import com.marcosgribel.movie_catalog.core.di.TestSchedulerProvider
import com.marcosgribel.movie_catalog.core.di.module.ServiceModuleTest
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCase
import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCaseImpl
import com.marcosgribel.movie_catalog.movie.presentation.presenter.ListMovieActivityPresenter
import com.marcosgribel.movie_catalog.movie.presentation.presenter.ListMovieActivityPresenterImpl
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivity
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivityView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@Module
class ListMovieModuleTest {


    @Provides
    internal fun provideListMovieActivityView(activity: ListMovieActivity): ListMovieActivityView = activity

    @Provides
    internal fun provideMovieUseCase(): MovieUseCase {
        return MovieUseCaseImpl(ServiceModuleTest().provideMovieService())
    }

    @Provides
    internal fun provideListMovieActivityPresenter(view: ListMovieActivityView,
                                                   useCase: MovieUseCase,
                                                   @TestSchedulerProvider scheduler: ScheduleProvider): ListMovieActivityPresenter {
        return ListMovieActivityPresenterImpl(view, useCase, scheduler)

    }

}