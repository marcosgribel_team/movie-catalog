package com.marcosgribel.movie_catalog.movie.presentation.presenter

import com.marcosgribel.movie_catalog.core.TestScheduler
import com.marcosgribel.movie_catalog.movie.di.ListMovieModuleTest
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivityView
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.*

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class ListMovieActivityPresenterImplTest {


    private var movieViewMock = Mockito.mock(ListMovieActivityView::class.java)
    private var movieUseCaseMock = ListMovieModuleTest().provideMovieUseCase()

    private lateinit var presenter: ListMovieActivityPresenter

    @Before
    fun setUp() {
        presenter = ListMovieActivityPresenterImpl(movieViewMock, movieUseCaseMock, TestScheduler())
    }


    @Test
    fun testGetListOfMovieWithSuccess() {

        presenter.getListOfMovie(1)
        presenter.getListOfMovie(2)
        Mockito.verify(movieViewMock, times(2)).addAll(Mockito.anyList())

    }

    @Test
    fun testSearchItemWithSuccess(){
        presenter.getListOfMovie("query", 1)
        Mockito.verify(movieViewMock, times(1)).addAll(Mockito.anyList())
    }

}