package com.marcosgribel.movie_catalog.movie.presentation.ui

import android.view.MenuItem
import android.widget.GridView
import android.widget.TextView
import com.marcosgribel.movie_catalog.AppTest
import com.marcosgribel.movie_catalog.BuildConfig
import com.marcosgribel.movie_catalog.R
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.fakes.RoboMenuItem

/**
 * Created by marcosgribel on 6/21/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = AppTest::class)
class ListMovieActivityTest {


    private lateinit var listMovieActivity: ListMovieActivity
    private lateinit var gridView: GridView

    @Before
    fun setUp() {
        listMovieActivity = Robolectric.setupActivity(ListMovieActivity::class.java)
        gridView = listMovieActivity.findViewById(R.id.grid_list_of_movie)
    }


    @Test
    fun assertElementsOnScreen() {

        gridView = listMovieActivity.findViewById(R.id.grid_list_of_movie)
        var searchView: MenuItem = RoboMenuItem(R.id.action_search)

        assertNotNull(gridView)
        assertNotNull(searchView)
    }



    @Test
    fun assertOnItemClickOpenMovieDetail() {
        gridView.smoothScrollToPosition(0)

        val adapter = gridView.adapter
        assert(adapter.count > 0)

        val itemViewAdapter = adapter.getView(0, null, gridView)
        gridView.performItemClick(itemViewAdapter, 0, adapter.getItemId(0))

        val detailMovieFragment= listMovieActivity.supportFragmentManager.findFragmentByTag(DetailMovieBottomSheetFragment.TAG) as DetailMovieBottomSheetFragment

        assertNotNull(detailMovieFragment)
        assert(detailMovieFragment.isAdded)

        val title = detailMovieFragment.dialog.findViewById<TextView>(R.id.text_title_movie)
        assertNotNull(title)
        assert(! (title?.text.toString().isEmpty()) )

        val overview = detailMovieFragment.dialog.findViewById<TextView>(R.id.text_overview_movie)
        assertNotNull(overview)
        assert(! (overview?.text.toString().isEmpty()) )

    }
}