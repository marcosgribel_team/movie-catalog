package com.marcosgribel.movie_catalog

import com.marcosgribel.movie_catalog.core.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return  DaggerAppComponent.builder()
                .application(this)
                .build()
    }


    override fun onCreate() {
        super.onCreate()
        configTimber()
    }


    private fun configTimber(){
        if(BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}