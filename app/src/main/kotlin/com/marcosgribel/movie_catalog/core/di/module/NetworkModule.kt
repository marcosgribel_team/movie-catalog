package com.marcosgribel.movie_catalog.core.di.module

import com.marcosgribel.movie_catalog.BuildConfig
import com.marcosgribel.movie_catalog.core.di.ApiBaseUrl
import com.marcosgribel.movie_catalog.core.di.ApiKey
import com.marcosgribel.movie_catalog.genre.model.service.GenreApi
import com.marcosgribel.movie_catalog.movie.model.service.MovieApi
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class NetworkModule {


    @Provides
    @ApiKey
    internal fun provideApiKey(): String = BuildConfig.API_KEY

    @Provides
    @ApiBaseUrl
    internal fun provideApiBaseUrl(): String = BuildConfig.BASE_API_URL

    @Provides
    @Reusable
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        var httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.NONE
        if (BuildConfig.DEBUG)
            httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.BODY

        return HttpLoggingInterceptor().apply {
            level = httpLoggingInterceptorLevel
        }
    }

    @Provides
    @Reusable
    internal fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(httpLoggingInterceptor)
                .build()

    }


    @Provides
    @Reusable
    internal fun provideMoshiConverterFactory(): MoshiConverterFactory {
        var moshi = Moshi.Builder()
        moshi.add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        return MoshiConverterFactory.create(moshi.build())
    }

    @Provides
    @Reusable
    internal fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()


    @Provides
    @Reusable
    internal fun provideRetrofit(
            @ApiBaseUrl apiBaseUrl: String,
            moshiConverterFactory: MoshiConverterFactory,
            rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
            okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .addConverterFactory(moshiConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .client(okHttpClient)
                .build()
    }


    @Provides
    @Singleton
    internal fun provideMovieApi(retrofit: Retrofit): MovieApi = retrofit.create(MovieApi::class.java)

    @Provides
    @Singleton
    internal fun provideGenreApi(retrofit: Retrofit): GenreApi = retrofit.create(GenreApi::class.java)


}