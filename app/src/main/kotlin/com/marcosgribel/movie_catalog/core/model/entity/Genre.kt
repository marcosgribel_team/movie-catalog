package com.marcosgribel.movie_catalog.core.model.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Parcelize
@Entity
data class Genre(
        @field:Json(name = "id")
        @PrimaryKey
        var id: Int,
        @field:Json(name = "name")
        var name: String
) : Parcelable
