package com.marcosgribel.movie_catalog.core.model.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import io.reactivex.Flowable

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Dao
interface GenreDao {

    @Query("SELECT * FROM Genre")
    fun getAll() : Flowable<List<Genre>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(genre: Genre)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg genre: Genre)

    @Query(" SELECT * FROM Genre WHERE id IN (:ids)")
    fun getGenreByIds(ids: List<Int>) : Flowable<List<Genre>>

}
