package com.marcosgribel.movie_catalog.core.model.entity

import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
//@Entity
//@TypeConverters(AppTypeConverters::class)
data class Movie(

        @field:Json(name = "id")
        @PrimaryKey
        var id: Int? = null,

        @field:Json(name = "overview")
        var overview: String? = null,

        @field:Json(name = "original_language")
        var originalLanguage: String? = null,

        @field:Json(name = "original_title")
        var originalTitle: String? = null,

        @field:Json(name = "video")
        var video: Boolean? = null,

        @field:Json(name = "title")
        var title: String? = null,

        @field:Json(name = "genre_ids")
        @Ignore
        var genreIds: List<Int>? = arrayListOf(),

        @field:Json(name = "poster_path")
        var posterPath: String? = null,

        @field:Json(name = "backdrop_path")
        var backdropPath: String? = null,

        @field:Json(name = "release_date")
        var releaseDate: String? = null,

        @field:Json(name = "vote_average")
        var voteAverage: Double? = null,

        @field:Json(name = "popularity")
        var popularity: Double? = null,

        @field:Json(name = "adult")
        var adult: Boolean? = null,

        @field:Json(name = "vote_count")
        var voteCount: Int? = null
) : Parcelable {

    var imageUrl: String? = null
            get() = "https://image.tmdb.org/t/p/w500/${this.backdropPath}"

}