package com.marcosgribel.movie_catalog.core.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.core.di.AppLanguage
import com.marcosgribel.movie_catalog.core.di.AppScheduler
import com.marcosgribel.movie_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.movie_catalog.core.model.db.AppDatabase
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import dagger.Module
import dagger.Provides
import java.util.*
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module(includes = [
    NetworkModule::class,
    DatabaseModule::class,
    ServiceModule::class
])
class AppModule {

    @Provides
    @Singleton
    @AppLanguage
    fun provideLanguage(): String {
        return Locale.getDefault().language
    }

    @Provides
    @Singleton
    @AppSchedulerProvider
    fun provideScheduler() : ScheduleProvider = AppScheduler()

}