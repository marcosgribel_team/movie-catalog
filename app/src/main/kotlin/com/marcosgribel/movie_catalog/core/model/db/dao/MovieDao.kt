package com.marcosgribel.movie_catalog.core.model.db.dao

import android.arch.persistence.room.Dao
import com.marcosgribel.movie_catalog.core.model.entity.Movie

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Dao
interface MovieDao {

}