package com.marcosgribel.movie_catalog.core.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by marcosgribel on 6/20/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
object DateUtil {


    val YYYY_MM_DD = "yyyy-MM-dd"


    private fun stringToDate(value: String, pattern: String): Date {
        val format = SimpleDateFormat(pattern).parse(value)
        return Date(format.time)

    }


   private fun stringToCalendar(value: String, pattern: String): Calendar {
        val date = stringToDate(value, pattern)
        var calendar = Calendar.getInstance()
        calendar.time = date
        return calendar

    }

    fun getYearFromDateString(value: String, pattern: String) : Int {
        val calendar = stringToCalendar(value, pattern)
        val year = calendar.get(Calendar.YEAR)
        return year
    }

}