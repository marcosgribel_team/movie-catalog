package com.marcosgribel.movie_catalog.core.di

import javax.inject.Qualifier

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */



@Qualifier
annotation class ApiKey


@Qualifier
annotation class ApiBaseUrl


@Qualifier
annotation class AppLanguage

@Qualifier
annotation class AppSchedulerProvider

@Qualifier
annotation class TestSchedulerProvider