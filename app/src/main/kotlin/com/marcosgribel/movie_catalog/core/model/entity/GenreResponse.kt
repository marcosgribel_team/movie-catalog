package com.marcosgribel.movie_catalog.core.model.entity

import com.squareup.moshi.Json

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
data class GenreResponse(
        @Json(name = "genres")
        var genres: List<Genre>?
)