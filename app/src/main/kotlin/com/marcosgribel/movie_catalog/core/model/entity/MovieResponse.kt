package com.marcosgribel.movie_catalog.core.model.entity

import com.squareup.moshi.Json

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
data class MovieResponse(
        @field:Json(name = "page")
        var page: Int,
        @field:Json(name = "total_pages")
        var totalPages: Int,
        @field:Json(name = "total_results")
        var totalResults: Int,
        @field:Json(name = "results")
        var results: List<Movie>
)