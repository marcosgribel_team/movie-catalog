package com.marcosgribel.movie_catalog.core.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.core.model.db.dao.MovieDao
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import com.marcosgribel.movie_catalog.core.model.entity.Movie

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Database(
        exportSchema = false,
        version = 2,
        entities = [
            Genre::class
        ]

)
abstract class AppDatabase : RoomDatabase() {
    
    abstract fun movieDao(): MovieDao

    abstract fun genreDao(): GenreDao

}