package com.marcosgribel.movie_catalog.core.model.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.provider.SyncStateContract.Helpers.insert
import io.reactivex.Completable

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface BaseDao<T> {

    @Insert(onConflict = REPLACE)
    fun insert(obj: T)

    @Insert(onConflict = REPLACE)
    fun insert(vararg obj: T)

    @Insert(onConflict = REPLACE)
    fun update(obj: T)

    @Delete
    fun delete(obj: T)

}

@Dao
abstract class BaseDaoImpl<T> : BaseDao<T> {

    fun save(obj: T): Completable {
        return Completable.fromAction {
            insert(obj)
        }
    }

    fun save(vararg obj: T): Completable {
        return Completable.fromAction {
            insert(*obj)
        }
    }

    fun remove(obj: T): Completable {
        return Completable.fromAction {
            delete(obj)
        }
    }

}