package com.marcosgribel.movie_catalog.core.di.module

import com.marcosgribel.movie_catalog.main.di.MainModule
import com.marcosgribel.movie_catalog.main.presentation.ui.MainActivity
import com.marcosgribel.movie_catalog.movie.di.DetailMovieBottomSheetFragmentModule
import com.marcosgribel.movie_catalog.movie.di.ListMovieModule
import com.marcosgribel.movie_catalog.movie.presentation.ui.DetailMovieBottomSheetFragment
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [
        MainModule::class
    ])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [
        ListMovieModule::class
    ])
    internal abstract fun bindListMovieActivity(): ListMovieActivity

    @ContributesAndroidInjector(modules = [
        DetailMovieBottomSheetFragmentModule::class
    ])
    internal abstract fun bindDetailMovieBottomSheetFragment(): DetailMovieBottomSheetFragment
}