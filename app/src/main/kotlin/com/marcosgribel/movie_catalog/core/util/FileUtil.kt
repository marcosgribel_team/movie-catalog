package com.marcosgribel.movie_catalog.core.util

import java.io.ByteArrayOutputStream
import java.io.InputStream

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
object FileUtil {


    fun readTextStream(inputStream: InputStream): String {
        var result = ByteArrayOutputStream()
        var buffer = ByteArray(1024)
        var length = inputStream.read(buffer)

        while (length != -1) {
            result.write(buffer, 0, length)
            length = inputStream.read(buffer)
        }
        return result.toString("UTF-8");
    }
}