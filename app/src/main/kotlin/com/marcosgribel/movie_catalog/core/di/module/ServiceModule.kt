package com.marcosgribel.movie_catalog.core.di.module

import com.marcosgribel.movie_catalog.genre.model.service.GenreService
import com.marcosgribel.movie_catalog.genre.model.service.GenreServiceImpl
import com.marcosgribel.movie_catalog.movie.model.service.MovieService
import com.marcosgribel.movie_catalog.movie.model.service.MovieServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */

@Module
class ServiceModule {


    @Provides
    internal fun provideMovieService(service: MovieServiceImpl) : MovieService = service

    @Provides
    internal fun provideGenreService(service: GenreServiceImpl) : GenreService = service


}