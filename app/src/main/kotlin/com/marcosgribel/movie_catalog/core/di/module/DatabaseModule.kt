package com.marcosgribel.movie_catalog.core.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.core.model.db.AppDatabase
import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.core.model.db.dao.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class DatabaseModule {


    @Provides
    @Singleton
    internal fun provideAppDatabase(app: Application): AppDatabase {
        return Room.databaseBuilder(app,
                AppDatabase::class.java,
                app.getString(R.string.app_database_name))
                .fallbackToDestructiveMigration()

                .build()
    }


    @Provides
    @Singleton
    internal fun provideMovieDao(database: AppDatabase): MovieDao = database.movieDao()

    @Provides
    @Singleton
    internal fun provideGenreDao(database: AppDatabase): GenreDao = database.genreDao()


}