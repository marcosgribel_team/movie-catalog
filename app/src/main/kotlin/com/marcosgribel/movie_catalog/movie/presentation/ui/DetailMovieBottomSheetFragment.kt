package com.marcosgribel.movie_catalog.movie.presentation.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.core.model.entity.Movie
import com.marcosgribel.movie_catalog.core.util.DateUtil
import com.marcosgribel.movie_catalog.movie.presentation.presenter.DetailMovieBottomSheetFragmentPresenter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_detail_movie.view.*
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/20/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface DetailMovieBottomSheetFragmentView {

    fun addGenre(genres: String)

}

class DetailMovieBottomSheetFragment : BottomSheetDialogFragment(), DetailMovieBottomSheetFragmentView, HasSupportFragmentInjector {


    @Inject
    lateinit var fragmentChildInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentChildInjector
    }

    @Inject
    lateinit var presenter: DetailMovieBottomSheetFragmentPresenter

    companion object {

        val TAG = DetailMovieBottomSheetFragment::class.java.simpleName.toString()

        fun newInstance(context: Context, movie: Movie): DetailMovieBottomSheetFragment {
            var fragment = DetailMovieBottomSheetFragment()

            var args = Bundle()
            args.putParcelable(context.getString(R.string.args_movie), movie)

            fragment.arguments = args
            return fragment
        }

    }

    private lateinit var movie: Movie

    private val options by lazy {
        RequestOptions()
                .override(800, 600)
    }


    private val rootView by lazy {
        LayoutInflater.from(context).inflate(R.layout.fragment_detail_movie, null)
    }


    private val onBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }
        }

        override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {

        }
    }


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movie = arguments!!.getParcelable(getString(R.string.args_movie))

    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)

        dialog?.setContentView(rootView)

        configurebottomSheetBehavior()
        configureFieldOfGender(movie)
        loadGenres(movie)
    }

    private fun configurebottomSheetBehavior() {
        val parentView = rootView.parent as View
        val layoutParams = parentView.layoutParams as CoordinatorLayout.LayoutParams
        val behavior = layoutParams.behavior
        if (behavior is BottomSheetBehavior) {
            behavior.setBottomSheetCallback(onBottomSheetBehaviorCallback)
        }
    }

    private fun configureFieldOfGender(movie: Movie) {

        val year = DateUtil.getYearFromDateString(movie.releaseDate!!, DateUtil.YYYY_MM_DD)

        Glide.with(context!!)
                .load(movie.imageUrl)
                .apply(options)
                .into(rootView.img_header_movie_detail)

        rootView.text_title_movie.text = movie.title
        rootView.text_year_movie.text = year.toString()
        rootView.text_overview_movie.text = movie.overview

    }


    private fun loadGenres(movie: Movie) {
        presenter.loadListOfGenresByIds(movie.genreIds)
    }

    override fun addGenre(genres: String) {
        rootView.text_genres_movie_detail.text = genres
    }
}

