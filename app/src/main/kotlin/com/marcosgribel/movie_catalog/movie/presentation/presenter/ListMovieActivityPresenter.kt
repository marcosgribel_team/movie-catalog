package com.marcosgribel.movie_catalog.movie.presentation.presenter

import com.marcosgribel.movie_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.movie_catalog.core.model.entity.MovieResponse
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCase
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivityView
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface ListMovieActivityPresenter {


    fun attach()

    fun dettach()

    fun getListOfMovie(page: Int)

    fun getListOfMovie(query: String, page: Int)

    fun onSearch(string: String)
    fun onSearchSubscribe()
    fun onSearchComplete()

}

class ListMovieActivityPresenterImpl @Inject constructor(
        private val view: ListMovieActivityView,
        private val useCase: MovieUseCase,
        @AppSchedulerProvider private val schedulers: ScheduleProvider
) : ListMovieActivityPresenter {

    private var compositeDisposable = CompositeDisposable()
    private lateinit var searchSubject: PublishSubject<String>

    init {

        val TAG = ListMovieActivityPresenterImpl::class.java.simpleName.toString()
        Timber.tag(TAG)

    }

    override fun attach() {
        searchSubject = PublishSubject.create()
    }

    override fun dettach() {
        if(compositeDisposable.isDisposed){
            compositeDisposable.clear()
        }

    }

    override fun getListOfMovie(page: Int) {
        var disposable = useCase.getListOfMovie(page)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.mainThread())
                .subscribe({
                    view.addAll(it.results)
                }, {
                    Timber.e(it)
                })

        compositeDisposable.add(disposable)
    }


    override fun getListOfMovie(query: String, page: Int) {
        var disposable = useCase.searchListOfMovie(query, page)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.mainThread())
                .subscribe({
                    view.addAll(it.results)
                }, {

                    Timber.e(it)
                })

        compositeDisposable.add(disposable)
    }


    override fun onSearch(string: String) {

        searchSubject.onNext(string)
    }

    override fun onSearchComplete() {
        searchSubject.onComplete()
    }


    override fun onSearchSubscribe() {
        var disposable = searchSubject
                .debounce(200, TimeUnit.MILLISECONDS)
                .filter(Predicate { it: String ->
                    return@Predicate it.isNotEmpty()
                })
                .distinctUntilChanged()
                .subscribeOn(schedulers.mainThread())
                .observeOn(schedulers.io())
                .switchMapMaybe {
                    return@switchMapMaybe useCase.searchListOfMovie(it, 1)
                }
                .doOnSubscribe {
                    view.resetList()
                }
                .observeOn(schedulers.mainThread())
                .subscribe({
                    view.addAll(it.results)
                }, {
                    Timber.e(it)
                })

        compositeDisposable.add(disposable)

    }

}