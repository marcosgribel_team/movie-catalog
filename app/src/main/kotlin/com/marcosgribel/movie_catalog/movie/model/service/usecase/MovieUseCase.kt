package com.marcosgribel.movie_catalog.movie.model.service.usecase

import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import com.marcosgribel.movie_catalog.core.model.entity.MovieResponse
import com.marcosgribel.movie_catalog.movie.model.service.MovieService
import io.reactivex.Flowable
import io.reactivex.Maybe
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface MovieUseCase {


    fun getListOfMovie(page: Int): Maybe<MovieResponse>

    fun searchListOfMovie(query: String, page: Int): Maybe<MovieResponse>

    fun getListOfGenreByIds( ids: List<Int>) : Flowable<List<Genre>>

}

class MovieUseCaseImpl @Inject constructor(var service: MovieService, val genreDao: GenreDao) : MovieUseCase {

    override fun getListOfMovie(page: Int): Maybe<MovieResponse> = service.getListOfMovie(page)

    override fun searchListOfMovie(query: String, page: Int): Maybe<MovieResponse> = service.searchListOfMovie(query, page)

    override fun getListOfGenreByIds(ids: List<Int>) : Flowable<List<Genre>> = genreDao.getGenreByIds(ids)

}