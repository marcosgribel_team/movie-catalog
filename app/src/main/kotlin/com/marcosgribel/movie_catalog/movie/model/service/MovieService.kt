package com.marcosgribel.movie_catalog.movie.model.service

import com.marcosgribel.movie_catalog.core.di.ApiKey
import com.marcosgribel.movie_catalog.core.di.AppLanguage
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import com.marcosgribel.movie_catalog.core.model.entity.Movie
import com.marcosgribel.movie_catalog.core.model.entity.MovieResponse
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface MovieService {


    fun getListOfMovie(page: Int): Maybe<MovieResponse>

    fun searchListOfMovie(query: String, page: Int): Maybe<MovieResponse>


}


class MovieServiceImpl @Inject constructor(var movieApi: MovieApi,
                                           @ApiKey var apiKey: String,
                                           @AppLanguage var language: String) : MovieService {


    override fun getListOfMovie(page: Int): Maybe<MovieResponse> {
        return movieApi
                .getMovies(apiKey, page, language, null)
    }


    override fun searchListOfMovie(query: String, page: Int): Maybe<MovieResponse> {
        return movieApi.searchMovie(apiKey, page, language, query, false)
    }
}