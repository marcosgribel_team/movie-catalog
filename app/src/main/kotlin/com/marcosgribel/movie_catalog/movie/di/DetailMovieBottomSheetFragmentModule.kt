package com.marcosgribel.movie_catalog.movie.di

import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCase
import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCaseImpl
import com.marcosgribel.movie_catalog.movie.presentation.presenter.DetailMovieBottomSheetFragmentPresenter
import com.marcosgribel.movie_catalog.movie.presentation.presenter.DetailMovieBottomSheetFragmentPresenterImpl
import com.marcosgribel.movie_catalog.movie.presentation.ui.DetailMovieBottomSheetFragment
import com.marcosgribel.movie_catalog.movie.presentation.ui.DetailMovieBottomSheetFragmentView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/24/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class DetailMovieBottomSheetFragmentModule {

    @Provides
    fun provideDetailMovieBottomSheetFragmentView(view: DetailMovieBottomSheetFragment): DetailMovieBottomSheetFragmentView = view

    @Provides
    fun provideDetailMovieBottomSheetFragmentPresenter(presenter: DetailMovieBottomSheetFragmentPresenterImpl): DetailMovieBottomSheetFragmentPresenter = presenter

    @Provides
    internal fun provideMovieUseCase(useCase: MovieUseCaseImpl): MovieUseCase = useCase
}