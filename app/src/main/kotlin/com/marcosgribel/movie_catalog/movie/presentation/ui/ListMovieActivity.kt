package com.marcosgribel.movie_catalog.movie.presentation.ui

import android.content.Context
import android.content.Intent

import android.os.Bundle
import android.support.v7.widget.SearchView

import android.view.Menu
import android.view.MenuItem
import android.widget.AbsListView
import android.widget.AdapterView

import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.core.model.entity.Movie
import com.marcosgribel.movie_catalog.movie.presentation.adapter.MovieGridAdapter
import com.marcosgribel.movie_catalog.movie.presentation.presenter.ListMovieActivityPresenter
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_list_movie.*
import kotlinx.android.synthetic.main.content_list_movie.*
import javax.inject.Inject


interface ListMovieActivityView {

    fun addAll(movies: List<Movie>)

    fun resetList()

}

class ListMovieActivity : DaggerAppCompatActivity(), ListMovieActivityView {


    @Inject
    lateinit var presenter: ListMovieActivityPresenter

    companion object {

        fun newIntent(context: Context): Intent {
            var intent = Intent(context, ListMovieActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK ; Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }

    }

    /*

        Attributes

     */

    private var currentPage: Int = 1

    private var totalItemPerPage: Int = 0

    private val totalItemToShowPerPage: Int = 20
    private var lastItemPositionVisible = 0

    private var isLoading: Boolean = false

    private var searchView: SearchView? = null


    private val adapter: MovieGridAdapter by lazy {
        MovieGridAdapter(baseContext)
    }


    /*

        Activity lifecycle

     */

    override fun onStart() {
        super.onStart()
        presenter.attach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_movie)
        setSupportActionBar(toolbar)

        initialize()
        loadData()

    }

    override fun onStop() {
        presenter.dettach()
        super.onStop()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_movie, menu)

        configureSearchMenu(menu!!)

        return super.onCreateOptionsMenu(menu)
    }


    /*
     *  Initialize the grid view of movies
     */
    private fun initialize() {
        grid_list_of_movie.adapter = adapter
        grid_list_of_movie.setOnScrollListener(onEndlessScrollListener())
        grid_list_of_movie.onItemClickListener = onMovieItemClickListener()

    }

    /*
     *  Add and configure search bar
     *
     */
    private fun configureSearchMenu(menu: Menu) {

        var menuSearch = menu?.findItem(R.id.action_search)
        menuSearch?.setOnActionExpandListener(onSearchMenuExpandListener())

        searchView = menuSearch!!.actionView as SearchView
        searchView?.setOnQueryTextListener(onSearchQueryTextListener())

    }


    private fun loadData() {
        resetList()
        loadMoreData()
    }

    private fun loadMoreData() {
        if ( (searchView != null) && searchView?.isFocused!!)
            presenter.getListOfMovie(searchView?.query.toString(), currentPage)
        else
            presenter.getListOfMovie(currentPage)
    }

    override fun addAll(movies: List<Movie>) {
        adapter.addAll(movies)
    }

    override fun resetList() {
        adapter.clear()
        currentPage = 1
    }


    /*


           Actions


     */


    private fun onSearchQueryTextListener(): SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter.onSearchComplete()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()) return true

                presenter.onSearch(newText!!)
                return true
            }
        }

    }


    private fun onSearchMenuExpandListener(): MenuItem.OnActionExpandListener {
        return object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                presenter.onSearchSubscribe()
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                presenter.onSearchComplete()
                loadData()
                return true
            }
        }
    }


    /**
     *  Behavior infinite/endless scroll
     *
     */
    private fun onEndlessScrollListener(): AbsListView.OnScrollListener {
        return object : AbsListView.OnScrollListener {
            override fun onScroll(view: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                val currentItemPositionVisible = view!!.firstVisiblePosition
                if (currentItemPositionVisible > lastItemPositionVisible) {
                    if (isLoading) {
                        if (totalItemCount > totalItemPerPage) {
                            isLoading = false
                            currentPage++
                        }
                    }

                    if ((!isLoading) &&
                            ((totalItemCount - visibleItemCount!!) <= (firstVisibleItem + totalItemToShowPerPage))) {
                        isLoading = true
                        loadMoreData()
                    }
                }
                lastItemPositionVisible = currentItemPositionVisible

            }

            override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) {

            }
        }

    }


    private fun onMovieItemClickListener(): AdapterView.OnItemClickListener {
        return AdapterView.OnItemClickListener { parent, view, position, id ->

            val item = adapter.getItem(position)
            val bottomSheetFragment = DetailMovieBottomSheetFragment.newInstance(this, item)
            bottomSheetFragment.show(supportFragmentManager, DetailMovieBottomSheetFragment.TAG)

        }
    }
}