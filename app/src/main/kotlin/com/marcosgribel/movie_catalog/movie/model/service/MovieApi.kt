package com.marcosgribel.movie_catalog.movie.model.service

import com.marcosgribel.movie_catalog.core.di.ApiKey
import com.marcosgribel.movie_catalog.core.model.entity.MovieResponse
import io.reactivex.Maybe
import io.reactivex.Single
import org.intellij.lang.annotations.Language
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface MovieApi {


    @GET("/3/movie/popular")
    fun getMovies(@Query("api_key") apiKey: String,
                  @Query("page") page: Int,
                  @Query("language") language: String? = "en-US",
                  @Query("sort_by") sortBy: String? = "release_date.asc"
    ): Maybe<MovieResponse>


    @GET("/3/search/movie")
    fun searchMovie(@Query("api_key") apiKey: String,
                    @Query("page") page: Int,
                    @Query("language") language: String? = "en-US",
                    @Query("query") query: String,
                    @Query("include_adult") includeAdult: Boolean = false

    ): Maybe<MovieResponse>

}