package com.marcosgribel.movie_catalog.movie.presentation.presenter

import com.marcosgribel.movie_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import com.marcosgribel.movie_catalog.movie.model.service.usecase.MovieUseCase
import com.marcosgribel.movie_catalog.movie.presentation.ui.DetailMovieBottomSheetFragmentView
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/24/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface DetailMovieBottomSheetFragmentPresenter {

    fun loadListOfGenresByIds(ids: List<Int>?)

}

class DetailMovieBottomSheetFragmentPresenterImpl @Inject constructor(val view: DetailMovieBottomSheetFragmentView,
                                                                      val useCase: MovieUseCase,
                                                                      @AppSchedulerProvider private val schedulers: ScheduleProvider) : DetailMovieBottomSheetFragmentPresenter {

    init {
        val TAG = DetailMovieBottomSheetFragmentPresenter::class.java.simpleName.toString()
        Timber.tag(TAG)
    }

    override fun loadListOfGenresByIds(ids: List<Int>?) {
        if (ids == null) return


        useCase.getListOfGenreByIds(ids)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.mainThread())
                .subscribe({
                   val str = it.joinToString(", ") {
                        it.name
                    }
                    view.addGenre(str)

                }, {
                    Timber.e(it)
                })
    }
}