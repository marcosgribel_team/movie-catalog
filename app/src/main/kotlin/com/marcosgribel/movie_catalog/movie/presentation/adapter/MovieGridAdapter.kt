package com.marcosgribel.movie_catalog.movie.presentation.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.core.model.entity.Movie
import kotlinx.android.synthetic.main.item_list_movie.view.*

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class MovieGridAdapter(var context: Context) : BaseAdapter() {


    inner class MovieViewHolder(var view: View) : RecyclerView.ViewHolder(view) {

        val options by lazy {
            RequestOptions()
                    .override(800, 600)
        }


        fun bindData(data: Movie) {

            Glide.with(view.img_header_movie.context)
                    .load(data.imageUrl)
                    .apply(options)
                    .into(view.img_header_movie)

        }

    }


    private var items: MutableList<Movie> = arrayListOf()


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var view: View?
        var holder: MovieViewHolder

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_list_movie, null)
            holder = MovieViewHolder(view)
        } else {
            view = convertView
            holder = MovieViewHolder(convertView)


        }

        holder.bindData(items[position])

        return view!!
    }

    override fun getItem(position: Int): Movie = items[position]

    override fun getItemId(position: Int): Long = items[position].id!!.toLong()

    override fun getCount(): Int = items.size

    fun addAll(movies: List<Movie>) {
        items.addAll(movies)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

}