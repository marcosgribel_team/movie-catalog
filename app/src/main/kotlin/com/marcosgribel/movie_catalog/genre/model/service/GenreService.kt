package com.marcosgribel.movie_catalog.genre.model.service

import com.marcosgribel.movie_catalog.core.di.ApiKey
import com.marcosgribel.movie_catalog.core.di.AppLanguage
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import io.reactivex.Maybe
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface GenreService {

    fun getAllGenre() : Maybe<List<Genre>>
}

class GenreServiceImpl @Inject constructor(var genreApi: GenreApi,
                                               @ApiKey var apiKey: String,
                                               @AppLanguage var language: String
) : GenreService {


    override fun getAllGenre(): Maybe<List<Genre>> {
        return genreApi.getGenres(apiKey, language).concatMap {
            Maybe.just(it.genres)
        }
    }

}