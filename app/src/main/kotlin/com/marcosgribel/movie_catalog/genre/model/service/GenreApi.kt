package com.marcosgribel.movie_catalog.genre.model.service

import com.marcosgribel.movie_catalog.core.model.entity.GenreResponse
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface GenreApi {


    @GET("/3/genre/movie/list")
    fun getGenres(@Query("api_key") apiKey: String,
                  @Query("language") language: String? = "en-US"
    ): Maybe<GenreResponse>

}