package com.marcosgribel.movie_catalog.main.presentation.preseneter

import com.marcosgribel.movie_catalog.core.di.AppSchedulerProvider
import com.marcosgribel.movie_catalog.core.util.providers.ScheduleProvider
import com.marcosgribel.movie_catalog.main.model.usecase.MainUseCase
import com.marcosgribel.movie_catalog.main.presentation.ui.MainView
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface MainPresenter {


    fun loadPreConfiguration()

    fun handleGlobalError(t: Throwable)

}

class MainPresenterImpl @Inject constructor(private val view: MainView,
                                            private val useCase: MainUseCase,
                                            @AppSchedulerProvider private val scheduler: ScheduleProvider
) : MainPresenter {


    init {
        val TAG = MainPresenterImpl::class.java.simpleName.toString()
        Timber.tag(TAG)
    }

    override fun loadPreConfiguration() {
        useCase.getAndCacheGenre()
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .subscribe({
                    view.startMovieActivity()
                }, {
                    handleGlobalError(it)
                })
    }

    override fun handleGlobalError(t: Throwable) {
        Timber.e(t)
        view.showGlobalErrorMessage()
    }

}