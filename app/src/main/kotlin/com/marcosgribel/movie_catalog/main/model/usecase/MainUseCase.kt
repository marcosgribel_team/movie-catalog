package com.marcosgribel.movie_catalog.main.model.usecase

import com.marcosgribel.movie_catalog.core.model.db.dao.GenreDao
import com.marcosgribel.movie_catalog.genre.model.service.GenreService
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface MainUseCase {

    fun getAndCacheGenre(): Completable

}

class MainUseCaseImpl @Inject constructor(
        private var genreService: GenreService,
        private var genreDao: GenreDao
) : MainUseCase {

    override fun getAndCacheGenre(): Completable {
        return genreService.getAllGenre().flatMapCompletable {

            Completable.fromAction {
                genreDao.insert(*it.toTypedArray())
            }
        }

    }

}