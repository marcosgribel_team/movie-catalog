package com.marcosgribel.movie_catalog.main.presentation.ui

import android.os.Bundle
import android.widget.Toast
import com.marcosgribel.movie_catalog.R
import com.marcosgribel.movie_catalog.main.presentation.preseneter.MainPresenter
import com.marcosgribel.movie_catalog.movie.presentation.ui.ListMovieActivity
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject


interface MainView {

    fun startMovieActivity()

    fun showGlobalErrorMessage()

}



class MainActivity : DaggerAppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.loadPreConfiguration()
    }


    override fun startMovieActivity() {
        startActivity(ListMovieActivity.newIntent(this))
        finish()
    }

    override fun showGlobalErrorMessage() {
        Toast.makeText(this, getString(R.string.message_global_error), Toast.LENGTH_SHORT).show()
    }

}
