package com.marcosgribel.movie_catalog.main.di

import com.marcosgribel.movie_catalog.main.presentation.ui.MainActivity
import com.marcosgribel.movie_catalog.main.presentation.preseneter.MainPresenter
import com.marcosgribel.movie_catalog.main.presentation.preseneter.MainPresenterImpl
import com.marcosgribel.movie_catalog.main.presentation.ui.MainView
import com.marcosgribel.movie_catalog.main.model.usecase.MainUseCase
import com.marcosgribel.movie_catalog.main.model.usecase.MainUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 6/18/18.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class MainModule {

    @Provides
    internal fun provideMainView(activity: MainActivity): MainView = activity

    @Provides
    internal fun provideMainUseCase(useCase: MainUseCaseImpl): MainUseCase = useCase

    @Provides
    internal fun provideMainPresenter(presenter: MainPresenterImpl): MainPresenter = presenter



}