package com.marcosgribel.movie_catalog.core.model.db

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import com.marcosgribel.movie_catalog.core.model.entity.Genre
import com.marcosgribel.movie_catalog.core.model.entity.GenreResponse
import com.marcosgribel.movie_catalog.core.util.FileUtil
import com.squareup.moshi.Moshi
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by marcosgribel on 6/19/18.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class AppDatabaseTest {

    @get:Rule
    open var instantTaskExecutor = InstantTaskExecutorRule()

    private var moshi = Moshi.Builder().build()

    private lateinit var appDatabase: AppDatabase
    private var fakeListOfGenre: List<Genre>? = null


    @Before
    fun setUp() {
        var targetContext = InstrumentationRegistry.getTargetContext()
        val context = InstrumentationRegistry.getContext()

        appDatabase = Room.inMemoryDatabaseBuilder(targetContext,
                AppDatabase::class.java)
                .allowMainThreadQueries()
                .build()

        buildFakeGenreResponse(context)
    }


    private fun buildFakeGenreResponse(context: Context) {
        val inputStream = context.resources.assets.open("genre.json")
        val genreJson = FileUtil.readTextStream(inputStream)


        val genreJsonAdapter = moshi.adapter(GenreResponse::class.java)
        var response = genreJsonAdapter.fromJson(genreJson)
        fakeListOfGenre = response?.genres


        Completable.fromAction {
            appDatabase.genreDao()
                    .insert(*fakeListOfGenre!!.toTypedArray())
        }
                .test()
                .assertComplete()

    }


    @Test
    fun testGenreInser() {
        val total = fakeListOfGenre?.size

        appDatabase.genreDao().getAll().test().assertValue {
            it.isNotEmpty() && it.size == total
        }
    }

    @Test
    fun testGetGenreFromIds(){

        var ids = arrayListOf(28, 18, 10751)

        appDatabase
                .genreDao()
                .getGenreByIds(ids)
                .test()
                .assertValue {
                    (it.size == ids.size) &&
                            (it.any { it.id == ids[0] })
                    (it.any { it.id == ids[2] })
                }

    }

    @After
    fun setDown(){
        appDatabase?.clearAllTables()
        appDatabase?.close()
    }
}